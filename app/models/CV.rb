class CV
  attr_accessor :city, :job, :skills, :languages

  def initialize(args = {})
    args.each { |key,value| send("#{key}=", value) }
  end
end
