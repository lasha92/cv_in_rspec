require 'spec_helper'
require 'rails_helper'
require 'CV'

describe CV do
  before(:each) do
    @jobSeeker = CV.new(:city => 'Tallinn', :job => 'Developer', :skills => %w(AngularJS Rails Rspec Cucumber C# HTML CSS Java),:languages => 'english')
  end
  describe 'personal data' do


    it 'should live in Tallinn' do
      expect(@jobSeeker.city).to eq('Tallinn')
    end

    it 'should be developer' do
      expect(@jobSeeker.job).to eq('Developer')
    end
  end

  describe 'programming skills' do
    it 'should know Angularjs C# HTML CSS' do
      expect(@jobSeeker.skills).to include('AngularJS', 'C#', 'HTML', 'CSS')
    end

    it 'also likes Rails, TDD, BDD' do
      expect(@jobSeeker.skills).to include('Rails','Rspec','Cucumber')
    end
  end

  describe 'Language' do
    it "needs to work on gaining more spearking words, but aside from that he knows english" do
      expect(@jobSeeker.languages).to include('english')
    end

  end
end
